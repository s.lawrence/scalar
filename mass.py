#!/usr/bin/env python

import numpy as np
import scipy as sp
import sys

lines = sys.stdin.readlines()
paramstr = lines[0].split()[1:]
lines = lines[1:]

D = int(paramstr[0])
L = int(paramstr[1])
N = int(paramstr[2])
beta = int(paramstr[3])

dat = np.array([[float(x) for x in l.split()] for l in lines])
samples = len(dat)

# The array dat is organized as follows. The first index is the sample. The
# second contains the channel, with all O(N) and spacetime indices. The final
# index is the time separation.
dat = dat.reshape((samples,3+N*2,beta))

# In the naming dat_ij, the first number is the O(N) rank and the second is the
# spacetime rank.

# The first index is the sample; the second is the channel. Then come any O(N)
# and spacetime indices, and finally the time separation.

dat_00, dat = dat[:,:3,:], dat[:,3:,:]
dat_10 = dat[:,:(2+N),:].reshape((samples,2,N,beta))

def make_correlator(dat):
    """ Obtain a correlator from the data for a channel. The returned array has
    three indices: the first two are channels, and the last is the time
    separation. All other indices have been averaged (sample) or summed over
    (O(N) and spacetime indices).
    """
    cor = np.zeros((dat.shape[1], dat.shape[1], dat.shape[-1]))
    internal = tuple(range(2,len(dat.shape)-1))
    for dt in range(dat.shape[-1]):
        dat_ = np.roll(dat, dt, axis=-1)
        for c1 in range(dat.shape[1]):
            for c2 in range(dat.shape[1]):
                pr = dat[:,c1,...]*dat_[:,c2,...]
                cor[c1,c2,dt] = np.mean(np.sum(pr,axis=internal), axis=(0,-1))
    return cor

cor_00 = make_correlator(dat_00)
cor_10 = make_correlator(dat_10)

if False:
    # Get naive mass plot.
    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(np.log(cor_00[0,0,:20]/cor_00[0,0,1:21]))
    plt.figure()
    plt.plot(np.log(cor_10[0,0,:20]/cor_10[0,0,1:21]))
    plt.show()

def eigenvalues(cor):
    r = np.zeros((cor.shape[0], cor.shape[2]))
    for dt in range(cor.shape[2]):
        vals, _ = np.linalg.eig(cor[:,:,dt])
        r[:,dt] = vals
    return r

ev_00 = eigenvalues(cor_00)
ev_10 = eigenvalues(cor_10)

if False:
    # Plot correlators
    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(ev_00[0,:])
    plt.plot(ev_00[1,:])
    plt.plot(ev_00[2,:])
    plt.figure()
    plt.plot(ev_10[0,:])
    plt.plot(ev_10[1,:])
    plt.show()

if False:
    # Get mass plot
    import matplotlib.pyplot as plt
    T = 20
    if False:
        plt.figure()
        plt.plot(np.log(ev_00[0,:T-1]/ev_00[0,1:T]))
        plt.plot(np.log(ev_00[1,:T-1]/ev_00[1,1:T]))
        plt.plot(np.log(ev_00[2,:T-1]/ev_00[2,1:T]))
    plt.figure()
    plt.plot(np.log(ev_10[0,:T-1]/ev_10[0,1:T]))
    plt.plot(np.log(ev_10[1,:T-1]/ev_10[1,1:T]))
    plt.show()

def list_masses(ev):
    from scipy.optimize import curve_fit
    skip = 1
    def fit_func(t,a,b,c):
        return a*np.cosh(b*(t - beta/2))+c
    t = np.arange(beta)
    masses = []
    for k in range(ev.shape[0]):
        fit,_ = curve_fit(fit_func,t[1+skip:-skip],ev[k,1+skip:-skip],bounds=([0,0,0],[np.inf,np.inf,np.inf]))
        masses.append(fit[1])
    if False:
        import matplotlib.pyplot as plt
        plt.plot(ev[k,:])
        plt.plot(t,fit_func(t,*fit))
        plt.show()
    return masses

if True:
    # Perform fits
    masses_00 = list_masses(ev_00)
    masses_10 = list_masses(ev_10)
    masses_01 = []
    for m in sorted(masses_00):
        print(f'0 0 : {m}')
    for m in sorted(masses_10):
        print(f'1 0 : {m}')
    for m in sorted(masses_01):
        print(f'0 1 : {m}')

