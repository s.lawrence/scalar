program observe

    ! Parameters
    character :: comment
    integer :: beta, D, L, V, N
    real :: m2, lambda

    ! Input
    integer :: stat
    real, allocatable :: phi(:,:,:)

    read *, comment, D, L, N, beta, m2, lambda

    if (comment /= '#') then
        stop "input format incorrect"
    end if

    print *, '#', D, L, N, beta, m2, lambda

    V = L**D

    allocate(phi(N,V,beta))
    do
        read (*,*,iostat=stat) phi
        if (stat /= 0) then
            exit
        end if
        call measure(phi)
    end do
    deallocate(phi)

contains

subroutine measure(phi)
    real, intent(in), allocatable :: phi(:,:,:)
    integer :: t
    real :: op_00(beta,3), op_10(beta,N,2), op_01(beta,D+1,0)

    do t = 1,beta
        op_00(t,1) = sum(phi(:,:,t)*phi(:,:,t))
        op_00(t,2) = sum(phi(:,:,t)*phi(:,:,t))**2
        op_00(t,3) = sum(sum(phi(:,:,t),dim=2)**2)
        op_10(t,:,1) = sum(phi(:,:,t),dim=2)
        op_10(t,:,2) = sum(phi(:,:,t)**3,dim=2)
    end do
    print *, op_00, op_10
end subroutine

end program
