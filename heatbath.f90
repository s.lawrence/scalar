program heatbath

    !use iso_fortran_env, only : error_unit

    ! Constants
    real, parameter :: pi = 3.141592653589793238462643383279

    ! Lattice parameters
    integer :: beta, D, L, V, N
    real :: m2, lambda
    character(len=80) :: betastr, Dstr, Lstr, Nstr, m2str, lambdastr

    ! Monte Carlo parameters
    integer :: Nsamp
    character(len=80) :: Nsampstr
    integer, parameter :: Ntherm = 200, Nsweep_V = 10
    integer Nsweep
    real del
    integer :: accepted, proposed

    ! Lattice MC
    real, allocatable :: phi(:,:,:)
    integer samp, sweep

    ! Argument parsing
    if (command_argument_count() /= 7) then
        stop "arguments: D L N beta m2 lambda Nsamp"
    end if

    call get_command_argument(1, Dstr)
    call get_command_argument(2, Lstr)
    call get_command_argument(3, Nstr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, m2str)
    call get_command_argument(6, lambdastr)
    call get_command_argument(7, Nsampstr)

    read (Dstr,*) D
    read (Lstr,*) L
    read (Nstr,*) N
    read (betastr,*) beta
    read (m2str,*) m2
    read (lambdastr,*) lambda
    read (Nsampstr,*) Nsamp

    V = L**D
    Nsweep = Nsweep_V*V*beta

    ! Output parameters
    print *, '#', D, L, N, beta, m2, lambda

    allocate(phi(N,V,beta))
    phi = 0.
    del = 0.5

    do samp = 1,Ntherm
        accepted = 0
        proposed = 0
        do sweep = 1,Nsweep
            call update(phi)
        end do
        if (samp > Ntherm/2) cycle
        if (accepted > proposed*0.5) del = del*1.1
        if (accepted < proposed*0.2) del = del*0.9
        !write (error_unit,'(F6.2)') accepted/real(nskip*nt*nx)*100.
    end do

    do samp = 1,Nsamp
        do sweep = 1,Nsweep
            call update(phi)
        end do
        print *, phi
    end do

    deallocate(phi)

contains

subroutine update(phi)
    real, allocatable, intent(inout) :: phi(:,:,:)
    real phip(N)
    real S, Sp, r
    integer x, t, i

    do x = 1,V
        do t = 1,beta
            ! Construct proposal
            do i = 1,N
                call random_normal(phip(i), del)
            end do
            phip = phi(:,x,t) + phip
            S = action_near(phi, phi(:,x,t), x, t)
            Sp = action_near(phi, phip, x, t)
            call random_number(r)
            if (r < exp(S - Sp)) then
                phi(:,x,t) = phip
                accepted = accepted + 1
            end if
            proposed = proposed + 1
        end do
    end do
end subroutine

pure function action_near(phi, phip, x, t) result(S)
    real, allocatable, intent(in) :: phi(:,:,:)
    real, intent(in) :: phip(N)
    integer, intent(in) :: x, t
    real :: z
    integer :: dir, tp, xp
    real :: S

    z = sum(phip*phip)
    S = 0.5*m2*z + lambda*z*z

    ! Time neighbors
    tp = mod(t,beta)+1
    if (tp /= t) then
        S = S + 0.5*sum((phip - phi(:,x,tp))**2)
    end if
    tp = mod(t-2+beta,beta)+1
    if (tp /= t) then
        S = S + 0.5*sum((phip - phi(:,x,tp))**2)
    end if

    ! All other neighbors
    do dir = 1,D
        xp = step(x,dir,1)
        if (xp /= x) then
            S = S + 0.5*sum((phip - phi(:,xp,t))**2)
        end if
        xp = step(x,dir,-1)
        if (xp /= x) then
            S = S + 0.5*sum((phip - phi(:,xp,t))**2)
        end if
    end do
end function

pure function step(x, dir, dist) result(z)
    integer, intent(in) :: x, dir, dist
    integer :: y, z, w, i, j, k

    y = x-1
    w = L**(dir-1)

    i = mod(y,w)
    j = mod(y,w*L)/w
    k = y/(w*L)

    j = mod(j+dist+L,L)

    z = i + w*j + k*w*L
    z = z+1
end function

! Sample two variables from the normal distribution with given mean and standard deviation. This
! function exists because, for the Box-Muller transform, it's more natural and efficient to sample
! two variables at once.
subroutine random_normals(x, y, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x, y
    real :: a, b, d, m
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_number(a)
    call random_number(b)
    a = a * 2 * pi
    b = - log(b)
    x = b*cos(a)*d + m
    y = b*sin(a)*d + m
end subroutine

! Sample from the normal distribution with given mean and standard deviation. The mean and deviation
! default to 0 and 1, respectively.
subroutine random_normal(x, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x
    real :: d, m, y
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_normals(x, y, d, m)
end subroutine

end program
