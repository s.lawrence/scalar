program worm

    ! Constants
    real, parameter :: pi = 3.141592653589793238462643383279

contains

! Sample two variables from the normal distribution with given mean and standard deviation. This
! function exists because, for the Box-Muller transform, it's more natural and efficient to sample
! two variables at once.
subroutine random_normals(x, y, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x, y
    real :: a, b, d, m
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_number(a)
    call random_number(b)
    a = a * 2 * pi
    b = - log(b)
    x = b*cos(a)*d + m
    y = b*sin(a)*d + m
end subroutine

! Sample from the normal distribution with given mean and standard deviation. The mean and deviation
! default to 0 and 1, respectively.
subroutine random_normal(x, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x
    real :: d, m, y
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_normals(x, y, d, m)
end subroutine

end program
