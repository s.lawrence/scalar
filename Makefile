FFLAGS = -O3 -Wall
FFLAGS += -fimplicit-none
#FFLAGS = -O0 -g -Wall

all: cluster heatbath observe worm

cluster: cluster.f90
	gfortran ${FFLAGS} -o $@ $<

heatbath: heatbath.f90
	gfortran ${FFLAGS} -o $@ $<

observe: observe.f90
	gfortran ${FFLAGS} -o $@ $<

worm: worm.f90
	gfortran ${FFLAGS} -o $@ $<

clean:
	${RM} cluster heatbath observe worm
