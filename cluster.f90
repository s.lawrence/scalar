program cluster 

    ! Constants
    real, parameter :: pi = 3.141592653589793238462643383279

    ! Lattice parameters
    integer :: D, L, V
    real :: m2, lambda
    character(len=80) :: Dstr, Lstr, m2str, lambdastr

    ! Monte Carlo parameters
    integer :: Nsamp
    character(len=80) :: Nsampstr
    integer, parameter :: Ntherm = 100, Nsweep = 4, Nskip = 10
    real del
    integer :: accepted, proposed

    ! Adjacencies
    integer, allocatable, dimension(:,:) :: adj

    ! Argument parsing
    if (command_argument_count() /= 5) then
        stop "arguments: D L m2 lambda Nsamp"
    end if

    call get_command_argument(1, Dstr)
    call get_command_argument(2, Lstr)
    call get_command_argument(3, m2str)
    call get_command_argument(4, lambdastr)
    call get_command_argument(5, Nsampstr)

    read (Dstr,*) D
    read (Lstr,*) L
    read (m2str,*) m2
    read (lambdastr,*) lambda
    read (Nsampstr,*) Nsamp
    
    V = L**D

    ! Prepare adjacency lists
    allocate(adj(V,2*D))
    block
        integer :: x, vol, dir, xi, xo, x_
        vol = 1
        do dir = 1,D
            do x = 1,V
                xi = mod(x-1,vol)
                x_ = (mod(x-1,vol*L))/vol
                xo = (x-1)/(vol*L)
                adj(x,2*dir-1) = xi + mod(x_-1+L,L)*vol + xo*(vol*L) + 1
                adj(x,2*dir) = xi + mod(x_+1+L,L)*vol + xo*(vol*L) + 1
            end do
            vol = vol*L
        end do
    end block

    del = 0.5
    accepted = 0
    proposed = 0
    call main

    deallocate(adj)

contains

subroutine main
    !use iso_fortran_env, only : error_unit

    real, dimension(V) :: phi
    integer :: step

    phi = 0.
    do step = 1,Ntherm*Nskip
        call update(phi)
        if (step > (ntherm/2)*nskip) cycle
        if (mod(step,nskip) == 0) then
            if (accepted > proposed*0.5) del = del*1.1
            if (accepted < proposed*0.2) del = del*0.9
            !write (error_unit,'(F6.2)') accepted/real(nskip*nt*nx)*100.
            accepted = 0
            proposed = 0
        end if
    end do

    do step = 1,Nsamp*Nskip
        call update(phi)
        if (mod(step,nskip) == 0) then
            print *, phi
        end if
    end do
end subroutine

pure function action(phi) result(S)
    real, dimension(V), intent(in) :: phi
    real :: S
    integer :: x, j, y

    S = sum(0.5*m2*phi*phi + lambda * phi*phi*phi*phi)
    do x = 1,V
        do j = 1,2*D
            y = adj(x,j)
            S = S + 0.25 * (phi(x) - phi(y))**2
        end do
    end do
end function

pure function action_near(phi, phip, x) result(S)
    real, intent(in) :: phi(V), phip
    integer, intent(in) :: x
    real :: S
    integer :: y, j

    S = 0.5*m2*phip*phip + lambda*phip*phip*phip*phip
    do j = 1,2*D
        y = adj(x,j)
        if (y /= x) then
            S = S + 0.5 * (phip - phi(y))**2
        end if
    end do
end function

subroutine update(phi)
    real, dimension(V), intent(inout) :: phi
    integer i
    do i = 1,Nsweep
        call sweep_metropolis(phi)
    end do
    call update_swendsen_wang(phi)
end subroutine

subroutine sweep_metropolis(phi)
    real, intent(inout) :: phi(V)
    real phip
    real S, Sp, r
    integer x

    do x = 1,V
        call random_normal(r, del/sqrt(real(V)))
        phip = phi(x) + r
        S = action_near(phi, phi(x), x)
        Sp = action_near(phi, phip, x)
        call random_number(r)
        if (r < exp(S - Sp)) then
            phi(x) = phip
            accepted = accepted + 1
        end if
        proposed = proposed + 1
    end do
end subroutine

subroutine update_swendsen_wang(phi)
    real, intent(inout) :: phi(V)
    logical b(V,2*D), done(V)
    integer :: x, dir, y
    real :: z, beta, p

    ! b: whether a link is active
    b = .false.
    ! done: whether a site has been visited
    done = .false.

    ! Construct clusters
    do x = 1,V
        do dir = 1,D
            y = adj(x,2*dir-1)
            if (phi(x)*phi(y) > 0) then
                call random_number(z)
                beta = phi(x)*phi(y)
                p = 1 - exp(-2*beta)
                if (z < p) then
                    b(x,2*dir-1) = .true.
                    b(y,2*dir) = .true.
                end if
            end if
        end do
    end do

    ! Flip clusters, half the time
    do x = 1,V
        if (done(x)) cycle
        call random_number(z)
        ! Perform flood fill
        call flood_flip(b,phi,done,x,z<0.5)
    end do
end subroutine

subroutine flood_flip(b, phi, done, x0, flip)
    real, intent(inout) :: phi(V)
    logical, intent(inout) :: done(V)
    logical, intent(in) :: b(V,2)
    integer, intent(in) :: x0
    logical, intent(in) :: flip
    integer queue(V), qfront, qback, x, xp, j

    ! Initialize the queue
    qfront = 1
    qback = 1
    queue(1) = x0
    done(x0) = .true.

    do while (qback >= qfront)
        x = queue(qfront)
        qfront = qfront + 1

        if (flip) phi(x) = -phi(x)

        do j = 1,2*D
            xp = adj(x,j)
            if (b(x,j) .and. .not. done(xp)) then
                qback = qback+1
                queue(qback) = xp
                done(xp) = .true.
            end if
        end do
    end do
end subroutine

! Sample two variables from the normal distribution with given mean and standard deviation. This
! function exists because, for the Box-Muller transform, it's more natural and efficient to sample
! two variables at once.
subroutine random_normals(x, y, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x, y
    real :: a, b, d, m
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_number(a)
    call random_number(b)
    a = a * 2 * pi
    b = - log(b)
    x = b*cos(a)*d + m
    y = b*sin(a)*d + m
end subroutine

! Sample from the normal distribution with given mean and standard deviation. The mean and deviation
! default to 0 and 1, respectively.
subroutine random_normal(x, dev, mean)
    real, intent(in), optional :: dev, mean
    real, intent(out) :: x
    real :: d, m, y
    d = 1
    m = 0
    if (present(dev)) d = dev
    if (present(mean)) m = mean
    call random_normals(x, y, d, m)
end subroutine

end program

