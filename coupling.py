#!/usr/bin/env python

# Compute bare coupling from renormalized parameters.

import numpy as np
import sys

D = int(sys.argv[1])
mu2 = float(sys.argv[2])
f = float(sys.argv[3])

assert D == 3

def loop_finite_2d(L, mu2):
    k1,k2 = np.meshgrid(np.arange(1,L+1), np.arange(1,L+1))
    g1 = 4*np.sin(np.pi*k1/L)**2
    g2 = 4*np.sin(np.pi*k2/L)**2
    return np.sum(1/(mu2 + g1 + g2)) / L**2

def loop_finite(L, mu2):
    k1,k2,k3 = np.meshgrid(np.arange(1,L+1), np.arange(1,L+1), np.arange(1,L+1))
    g1 = 4*np.sin(np.pi*k1/L)**2
    g2 = 4*np.sin(np.pi*k2/L)**2
    g3 = 4*np.sin(np.pi*k3/L)**2
    return np.sum(1/(mu2 + g1 + g2 + g3)) / L**3

def loop_limit():
    """ In the infinite volume limit. """
    # TODO
    pass

for L in range(200):
    print(loop_finite(L,0.2))

# TODO
